# Table of Contents

[[_TOC_]]

## Containers

Morello CI uses containers located in the [Morello CI containers repository](https://git.morello-project.org/morello/morello-ci-containers)

## Pipelines
Morello pipelines are located in [Morello CI pipelines repository](https://git.morello-project.org/morello/morello-ci-pipelines).
To enable a repository to use a pipeline, configure the CI configuration file in the repository setting to point to the desired pipeline. More info on GitLab documentation [here](https://docs.gitlab.com/ee/ci/pipelines/settings.html#specify-a-custom-cicd-configuration-file). 

The pipelines are:
### Firmware
- [EDKII](https://git.morello-project.org/morello/morello-ci-pipelines/-/blob/main/.gitlab-ci-edk2.yml)
- [SCP](https://git.morello-project.org/morello/morello-ci-pipelines/-/blob/main/.gitlab-ci-scp.yml)
- [TF-A](https://git.morello-project.org/morello/morello-ci-pipelines/-/blob/main/.gitlab-ci-tf-a.yml)

### Toolchain
- [Toolchain](https://git.morello-project.org/morello/morello-ci-pipelines/-/blob/main/.gitlab-ci-toolchain.yml)
- [GDB](https://git.morello-project.org/morello/morello-ci-pipelines/-/blob/main/.gitlab-ci-gdb.yml)

### OS
- [Android](https://git.morello-project.org/morello/morello-ci-pipelines/-/blob/main/.gitlab-ci-android.yml)
- [Busybox](https://git.morello-project.org/morello/morello-ci-pipelines/-/blob/main/.gitlab-ci.yml)

### Miscellaneous
- [Nightly scheduled pipeline](https://git.morello-project.org/morello/morello-ci-pipelines/-/blob/main/.gitlab-ci.yml)
- [User triggered pipeline for related merge requests](https://git.morello-project.org/morello/morello-ci-pipelines/-/blob/main/.gitlab-ci-poll-trigger.yml)

## Morello CI pipelines features
### 1. Support for patchsets across multiple repositories
Users can use labels to earmark a group of related merge requests (MRs) across different repositories to enable triggering a pipeline to build them. The [morello-ci-trigger](https://git.morello-project.org/morello/morello-ci-trigger) project runs a scheduled pipeline that polls for **"trigger-pipeline"** label along with a label with a prefix **"group-"** on any MRs and triggers the pipeline on **manifest** project with details of all the MRs that have the same label "group-*".
#### Workflow
1. Pass "-o ci.skip" to git push command while pushing the branch. This will skip the pipeline for the branch.
```
git push -o ci.skip origin test
warning: redirecting to https://git.morello-project.org/morello/android/platform/build/soong.git/
Enumerating objects: 4, done.
Counting objects: 100% (4/4), done.
Delta compression using up to 16 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 297 bytes | 297.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
remote:
remote: To create a merge request for test, visit:
remote:   https://git.morello-project.org/morello/android/platform/build/soong/-/merge_requests/new?merge_request%5Bsource_branch%5D=test
remote:
To https://git.morello-project.org/morello/android/platform/build/soong
 + 11fc1a8b...0e17c08a test -> test (forced update)
```
2. Go to the link to create MR link from above command output:
3. Add (Create if not present) the label "ci skip" in Labels box in the create merge request page and create the merge request. Here is an [example MR](https://git.morello-project.org/morello/android/platform/build/soong/-/merge_requests/20) with "ci skip" label. This will skip the pipeline for the MR that is created.
4. Create and add a "group-\<feature\>" label across all the merge requests that are interdependent.
5. (Skip this step if patch is not inteded for testing on a release Manifest branch)
	
	Create and add label with release manifest branch name "manifest_branch=\<release-branch\>" on the same MR that you will add "trigger-pipeline" label to in the following step.

6. Once ready for testing, Add the label "trigger-pipeline" on the MR from step 5 ( OR any MR if you have skipped step 5).
7. A pipeline is run on manifest project and a label "triggered" is posted on all the MR.
8. A comment is posted back on all the MR as "pipeline finished successfully" after the pipeline passes
9. The developer can then go ahead and merge individual MRs.

#### Notes
  - Only create labels in the form of "group-*" to group the MRs.
  - Once a pipeline is triggered,a "triggered" label is added to the MRs.
  - If the user decides to re-trigger a pipeline, delete the "triggered" label that is added automatically and re-create the "trigger-pipeline" label again.
  - If another MR is later decided to be included to the same group after the pipeline has been triggered for a set of MRs, either:

      1. terminate the already triggered pipeline, add the same "group-*" label to the MR, delete the "triggered" label from the MR where "trigger-pipeline" was previously added and re-create another "trigger-pipeline" label. The scheduled trigger job to will kick in, or,
      2. wait for the triggered pipeline to finish, tag the new MR with the group label, delete the "triggered" label and re-create "trigger-pipeline" label again on any MRs from the set.
  - label color is irrelevant
  - creating labels will clutter over time the dropdown for labels on the project. It requires maintainance to clean up obsolete labels.
##### **Example**
- The link to the pipeline is posted on all the MR back as shown below.
    * https://git.morello-project.org/morello/morello-ci-containers/-/merge_requests/8#note_10368
- Once the pipeline finishes, the pipeline Success/Failure is posted back as a comment on all the MR back as shown below.
    * https://git.morello-project.org/morello/morello-ci-containers/-/merge_requests/8#note_10450

### 2. User defined tests
Users can selectively specify test(s) to be run for a manually triggered pipeline.

#### Workflow
1. Go to CI/CD --> pipelines on the left side menu on the screen.
2. Click "run pipeline" button. Once clicked, the user will see this [view](https://git.morello-project.org/morello/morello-ci-pipelines/-/pipelines/new).
3. Specify **'USER_DEFINED_TESTS'** as the _"input variable key"_ .
4. Specify a space separated string with the desired tests names in the _"input variable value"_ like so:
```
android-binder android-compartment android-device-tree android-dvfs
android-logd android-multicore android-bionic
```
5. Click "Run pipeline" to trigger a pipeline that will run the tests specified by the user.

#### Notes
Only the tests available can be specified. For reference, see the [LAVA templates](https://git.morello-project.org/morello/morello-ci-pipelines/-/tree/main/lava/templates).

### 3. Test Parallelisation and Dynamically Generated Pipelines in Test Phase.
[Test Phase](https://git.morello-project.org/morello/morello-ci-pipelines/-/pipelines/9299) in the pipelines triggers children pipelines, one for each test. This helps reduce execution time rather than having all tests done in the same parent pipeline process. Please note that:
  - Multiple dynamically created test pipelines are considered created successfully when you see children created under **Downstream** like in this [view](https://git.morello-project.org/morello/morello-ci-pipelines/-/pipelines/9299)
  - Success or failure of tests submitted has no relevance to how parallelisation works. If tests failed then it will need some investigation on LAVA side and it doesn't mean that parallelised tests were not created.
  - Parallel children test pipelines are always created by default for any pipeline that has test phase. Even if user manually triggered a pipeline specifying only one test to be run, the pipeline for the test will be generated dynamically in the same way as if there were multiple tests.

### 4. Submitting tests to LAVA for validation
All tests are submitted to the [Morello LAVA instance](https://lava.morello-project.org/) for validation.

### 5. Release Manifest Testing
#### Manual Pipeline Trigger
1. Go to CI/CD --> pipelines on the left side menu on the screen.
2. Click "run pipeline" button. Once clicked, the user will see this [view](https://git.morello-project.org/morello/manifest/-/pipelines/new)
3. Please select the right Release branch or tag and click "Run Pipeline"

#### Testing Merge requests for a project against Release branch of the manifest.
All merge requests are tested against morello/mainline branch of the manifest by default. Please follow the steps mentioned below if you wish to test against a different manifest branch (example: morello/release-1.2)
1. Pass '-o ci.variable="MANIFEST_BRANCH=morello/release-1.2"' to "git push" command while you push the branch which is used to create the MR.
```
git push -o ci.variable="MANIFEST_BRANCH=morello/release-1.2" origin fix_feature
```
repo tool will use that specific manifest branch during repo init.

2. Create the MR as usual.

#### Testing Interdependent Merge Request against Release manifest branch using labels
Follow the steps from [workflow here](https://git.morello-project.org/morello/morello-ci-documentation/-/tree/main#workflow)

##### Notes
```
We use morello/mainline as default branch for interdependent patch
testing.

We can now force testing on release manifest branch for interdependent patches
by adding one more label "manifest_branch=<release_branch>" before
applying trigger-pipeline label on the same MR.

Same group-label workflow has to be followed but you need to add the
above label additionally to force testing on release branches.

Incase if the manifest branch specified doesn't exist, you get this comment:
https://git.morello-project.org/morello/morello-ci-containers/-/merge_requests/8#note_14985

If the branch exists, the pipeline is triggered and you get this
comment:
https://git.morello-project.org/morello/morello-ci-containers/-/merge_requests/8#note_14989

The firmware job for the pipeline from above comment is here:
https://git.morello-project.org/morello/manifest/-/jobs/51546/raw

It clearly shows the below log indicating that testing was triggered on
release branch:
repo init --depth=1 --no-tags --no-clone-bundle -u https://git.morello-project.org/morello/manifest.git

 -b morello/release-1.2 -g bsp --repo-rev=v2.16
```

## Understanding pipeline results
Normally the pipelines would fail in the test phase if any test has failed in LAVA, but because there were so many infrastructure and timeout related errors,
this can confuse users and make discerning the type of error hard to spot. (ideally failures should only be in the tests results as expected).
Although timeouts and other infra errors have decreased recenlty after moving FVPs to their own server, here is how to read the pipeline log to distinguish if
failure is from lava test or infrastructure.

A successful pipeline showing passing results

<img src="images/successful.png" alt="Successful Pipeline" width="70px" align="left">

A failed pipeline due to a failing test in LAVA (ie genuine error). Failed test circled in red.

<img src="images/failed.png" alt="Successful Pipeline" width="70px" align="left">

Any other type of error means it is an infrastructure error.

## References

### GitLab documentation
- [Understanding GitLab pipelines](https://docs.gitlab.com/ee/ci/yaml/index.html)
- [How to set pipeline config file for a repository](https://docs.gitlab.com/ee/ci/pipelines/settings.html#specify-a-custom-cicd-configuration-file)
- [List of GitLab predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)

### LAVA documention
- [LAVA](https://lava.morello-project.org/static/docs/v2/index.html)
